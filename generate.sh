for i in in/*; do
    filename="${i##*/}"
    folio=10#`echo $filename | cut -d "_" -f 1`
    counter=0
    echo "Processing ${filename}";
    # echo $folio;
    for page in $(identify $i | cut -d " " -f 1); do
        let num=$folio+$counter;
        let counter++;
        outname=$(printf "out/%03d.png" $num);
        # echo $num;
        echo $outname;
        # echo $page;
        convert -thumbnail x300 "${page}" "${outname}";
    done;
done
